//
//  PokemonViewModel.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI
import Foundation

class PokemonViewModel: ObservableObject {
    
    @Published var items: [Pokemon] = []
    @Published var isShowingLoading = true
    @Published var errorMessage: String = ""
    
    @Published var showAlert: Bool? = false

    func filter(by name: String) -> [Pokemon] {
        return self.items.filter {
            name.isEmpty ? true : $0.name.lowercased().contains(name.lowercased())
        }
    }
    
    func getPokemon() {
        self.isShowingLoading = true
        self.showAlert = false
        ApiService.consumeService(baseUrl: ApiService.baseUrl) { response in
            switch response {
            case .success(let data):
                self.isShowingLoading = false
                let response = data.deserialize(type: PokemonResponse.self)?.results
                self.items = response!.enumerated().map { (index, element) in
                    return Pokemon(id: index + 1, name: element.name, image: nil)
                }
            case .failure(let failure):
                print(failure)
                self.isShowingLoading = false
                self.errorMessage = failure.localizedDescription
                self.showAlert = true
            }
        }
    }
}


