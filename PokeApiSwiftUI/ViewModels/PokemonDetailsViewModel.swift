//
//  PokemonDetailsViewModel.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI
import Foundation

class PokemonDetailsViewModel: ObservableObject {
    @Published var detail: PokemonDetail?
    var pokemon: Pokemon
    
    init(pokemon: Pokemon){
        self.pokemon = pokemon
    }
    
    func getDetail(){
        let url = "\(ApiService.detail)\(pokemon.id!)"
        ApiService.consumeService(baseUrl: url) { response in
            switch response {
            case .success(let data):
                self.detail = data.deserialize(type: PokemonDetail.self)
                self.detail?.name = self.detail?.name.capitalized ?? ""
            case .failure(let failure):
                print(failure)
            }
        }
    }
}
