//
//  DragState.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 15/03/21.
//

import SwiftUI
import CoreGraphics

enum DragState {
    case none
    case pressing
    case dragging(traslation: CGSize)
    
    var transalation: CGSize{
        switch self {
        case .none, .pressing:
            return .zero
        case .dragging(let traslation):
            return traslation
        }
    }
    
    var isPressing: Bool {
        switch self {
        case .pressing, .dragging:
            return true
        case .none:
            return false
        }
    }
    
    var isDragging: Bool {
        switch self {
        case .dragging:
            return true
        default:
            return false
        }
    }
}
