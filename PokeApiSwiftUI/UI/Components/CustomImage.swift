//
//  CustomImage.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

struct CustomImage: View {
    var image: Image?
    var width: CGFloat?
    var height: CGFloat?

    init(image: Image, width: CGFloat = 400, height: CGFloat = 400){
        self.image = image
        self.width = width
        self.height = height
    }
    
    var body: some View {
        image?.frame(width: width, height: height)
            .scaledToFit()
            .clipShape(Circle())
            .shadow(radius: 10)
            .background(Circle().fill(Color.white))
    }
}

struct CircleImage_Preview: PreviewProvider {
    static var previews: some View {
        CustomImage(image: Image("nav_background"))
    }
}
