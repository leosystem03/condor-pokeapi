//
//  LoadableImageView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

struct LoadableImageView: View {
    private var data: Data?
    
    var stateContent: AnyView {
        if let data = data, let image = UIImage(data: data) {
            return AnyView(
                Image(uiImage: image).resizable()
            )
        } else {
            return AnyView(
                ActivityIndicator(isAnimating: .constant(true),
                                  style: .medium)
            )
        }
    }
    
    init(with data: Data?){
        self.data = data
    }
    
    func image (from data: Data) -> UIImage {
        UIImage(data: data) ?? UIImage()
    }
    var body: some View {
        HStack {
            stateContent
        }
    }
}

#if DEBUG
struct LoadableImageView_Previews: PreviewProvider {
    static var previews: some View {
        LoadableImageView(with: Data())
    }
}
#endif
