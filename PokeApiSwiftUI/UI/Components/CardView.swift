//
//  CardView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 14/03/21.
//

import SwiftUI

struct CardView: View {
    @ObservedObject var fetcher = ImageFetcher()
    
    let id = UUID()
    
    var pokemon: Pokemon
    
    var body: some View {
        
        TinderImage(image: fetcher.data == nil ? Image("background") : Image(uiImage: UIImage(data: fetcher.data!)!))
            .frame(minWidth: 0, maxWidth: .infinity)
            .cornerRadius(20)
            .background(Color.white)
            .padding(.horizontal, 20)
            .overlay(
                VStack{
                    Text(pokemon.name.capitalized)
                        .font(.system(.headline, design: .rounded))
                        .fontWeight(.bold)
                        .padding(.horizontal, 20)
                        .padding(.vertical, 20)
                        .background(Color.blue)
                        .cornerRadius(20)
                }.padding([.top, .horizontal], 20), alignment: .top
            )
        
        .onAppear{
            self.fetcher.getImage(id: self.pokemon.id!, url: ApiService.baseImg)
        }
        
        
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(pokemon: Pokemon(id: 0, name: "PICACHU", image: Data()))
    }
}
