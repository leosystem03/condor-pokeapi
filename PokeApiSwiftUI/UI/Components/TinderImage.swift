//
//  TinderImage.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 14/03/21.
//

import SwiftUI

struct TinderImage: View {
    var image: Image?
    
    var body: some View {
        image?
            .frame(minWidth:0, maxWidth: .infinity)
            .scaledToFill()
            .cornerRadius(20)
            .padding(.horizontal, 20)
    }
}

struct TinderImage_Previews: PreviewProvider {
    static var previews: some View {
        TinderImage()
    }
}
