//
//  PokemonRow.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

struct PokemonRow: View {
    var pokemon: Pokemon
    @ObservedObject var fetcher = ImageFetcher()
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
    }
    
    var body: some View {
        HStack {
            LoadableImageView(with: self.fetcher.data)
                .frame(width: 80.0, height: 80.0)
            VStack(alignment: .leading) {
                Text(pokemon.name).font(.headline)
                Text(String(format: "%03d", pokemon.id!)).font(.subheadline)
            }
        }.onAppear {
            self.fetcher.getImage(id: self.pokemon.id!, url: ApiService.baseImg)
        }
    }
}
