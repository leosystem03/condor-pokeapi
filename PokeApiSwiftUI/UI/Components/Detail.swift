//
//  TypeDetail.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

struct Detail: View {
    var text: String
    
    init(text: String){
        self.text = text
    }
    
    var body: some View {
        Text(self.text.capitalized)
            .font(.headline)
            .fontWeight(.semibold)
            .foregroundColor(Color.gray)
            .padding(8)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color.gray)
            )
    }
}

struct Detail_Previews: PreviewProvider {
    static var previews: some View {
        Detail(text: "DSASADAS")
    }
}
