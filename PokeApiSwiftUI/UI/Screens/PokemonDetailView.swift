//
//  PokemonDetailView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

struct PokemonDetailView: View {
    @ObservedObject var viewModel: PokemonDetailsViewModel
    @ObservedObject var fetcher = ImageFetcher()
    @ObservedObject var fetcherEsmerald = ImageFetcher()
    @ObservedObject var fetcherRuby = ImageFetcher()
    @ObservedObject var fetcherRed = ImageFetcher()
    
    var types: [Types] = []
    var limit = 5
    
    init(pokemon: Pokemon) {
        self.viewModel = PokemonDetailsViewModel(pokemon: pokemon)
        UITableView.appearance().indicatorStyle = .default
    }
    
    var body: some View {
        ScrollView {
            VStack (alignment: .center, spacing: 10){
                CustomImage(image: fetcher.data == nil ? Image("background") : Image(uiImage: UIImage(data: fetcher.data!)!))
                    .aspectRatio(contentMode: .fit)
                Text(viewModel.detail?.name != nil ? viewModel.detail!.name : "PokemonName")
                    .font(.largeTitle)
                    .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    .fontWeight(.black)
                HStack {
                    Text("Pokedex number: ")
                        .font(.body)
                        .foregroundColor(.black)
                        .fontWeight(.heavy)
                    Text("\(viewModel.detail?.order ?? 0)")
                }
                HStack {
                    HStack {
                        Text("Height: ")
                            .fontWeight(.black)
                        Text("\(viewModel.detail?.height ?? 0) m")
                    }
                    HStack {
                        Text("Weight: ")
                            .fontWeight(.black)
                        Text("\(viewModel.detail?.weight ?? 0) kg")
                    }
                }
                
                VStack (alignment: .leading){
                    Text("Types: ")
                        .fontWeight(.black)
                    ScrollView {
                        LazyHStack {
                            ForEach(viewModel.detail?.types ?? [], id: \.self) { type in
                                Detail(text: type.type.name)
                            }
                        }
                    }.frame(height: 60)
                }
                .padding(10)
                .frame(width: 300, height: 100, alignment: .leading)
                
                VStack (alignment: .leading){
                    Text("Moves: ")
                        .fontWeight(.black)
                    List(viewModel.detail?.moves.prefix(5) ?? [], id: \.self){ move in
                        Detail(text: move.move.name)
                    }
                }
                .padding(10)
                .frame(width: 300, height: 300, alignment: .leading)
                VStack{
                    Text("Sprites: ")
                        .fontWeight(.black)
                    HStack{
                        CustomImage(image: fetcherEsmerald.data == nil ? Image("background") : Image(uiImage: UIImage(data: fetcherEsmerald.data!)!), width: 80, height: 80)
                            .aspectRatio(contentMode: .fit)
                        CustomImage(image: fetcherRuby.data == nil ? Image("background") : Image(uiImage: UIImage(data: fetcherRuby.data!)!), width: 80, height: 80)
                            .aspectRatio(contentMode: .fit)
                        CustomImage(image: fetcherRed.data == nil ? Image("background") : Image(uiImage: UIImage(data: fetcherRed.data!)!), width: 80, height: 80)
                            .aspectRatio(contentMode: .fit)
                    }
                }
            }
        }
        .navigationBarTitle(self.viewModel.detail?.name ?? "", displayMode: .inline)
        .onAppear {
            self.viewModel.getDetail()
            self.fetcher.getImage(id: self.viewModel.pokemon.id!, url: ApiService.baseImg)
            self.fetcherEsmerald.getImage(id: self.viewModel.pokemon.id!, url: ApiService.emerald)
            self.fetcherRuby.getImage(id: self.viewModel.pokemon.id!, url: ApiService.ruby)
            self.fetcherRed.getImage(id: self.viewModel.pokemon.id!, url: ApiService.red_blue)
        }
        
    }
}

struct PokemonDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonDetailView(pokemon: Pokemon(id: 0, name: "ASdfsd", image: Data()))
    }
}
