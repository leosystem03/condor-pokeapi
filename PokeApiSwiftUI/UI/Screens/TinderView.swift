//
//  TinderView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 14/03/21.
//

import SwiftUI

struct TinderView: View {
    
    private let threshold: CGFloat = 80
    @GestureState private var dragState = DragState.none
    
    var listPokemon: [Pokemon]
    @State var newListPokemonLike: [Pokemon] = []
    
    @State var deck = [CardView]()
    @State private var lastCardIdx = 1
    
    @State private var removalTransition = AnyTransition.leadingBottomAtRemoval
    
    init(listPokemon: [Pokemon]) {
        self.listPokemon = listPokemon
    }
    
    var body: some View {
        Spacer(minLength: 10)
        NavigationLink(destination: ListLikeView(listPokemon: self.newListPokemonLike)){
            Detail(text: "Favorite Pokemon")
        }
        ZStack {            
            ForEach (deck, id: \.id ) { card in
                card
                    .zIndex(self.isTopCard(card: card) ? 1: 0)
                    //mueve la carta
                    .overlay(
                        ZStack{
                            Image(systemName: "xmark.circle.fill")
                                .foregroundColor(.blue)
                                .font(.system(size: 120))
                                .opacity(self.isTopCard(card: card) && self.dragState.transalation.width <  -self.threshold ? 1 : 0)
                            
                            Image(systemName: "heart.circle.fill")
                                .foregroundColor(.blue)
                                .font(.system(size: 120))
                                .opacity(self.isTopCard(card: card) && self.dragState.transalation.width >  self.threshold ? 1 : 0)
                        }
                    )
                    .offset(x: self.isTopCard(card: card) ? self.dragState.transalation.width : 0,
                            y: self.isTopCard(card: card) ? self.dragState.transalation.height : 0)
                    //la hace pequeña
                    .scaleEffect(self.dragState.isDragging && self.isTopCard(card: card) ? 0.9 : 1.0)
                    //Rota la carta
                    .rotationEffect(Angle(degrees: Double(self.isTopCard(card: card) ? self.dragState.transalation.width/10 : 0)))
                    .animation(.interpolatingSpring(stiffness: 200, damping: 100))
                    .transition(self.removalTransition)
                    .gesture(LongPressGesture(minimumDuration: 0.01)
                                .sequenced(before: DragGesture())
                                .updating(self.$dragState, body: { (value, state, transaction) in
                                    switch value{
                                    case .first(true):
                                        state = .pressing
                                    case .second(true, let drag):
                                        state = .dragging(traslation: drag?.translation ?? .zero)
                                    default:
                                        break
                                    }
                                })
                                .onChanged{ (value) in
                                    guard case .second(true, let drag?) = value else {
                                        return
                                    }
                                    
                                    self.removalTransition = drag.translation.width > 0 ? .trailingBottomAtRemoval : .leadingBottomAtRemoval
                                }
                                .onEnded{(value) in
                                    guard case .second(true, let drag?) = value else {
                                        return
                                    }
                                    
                                    if drag.translation.width > self.threshold {
                                        newListPokemonLike.append(deck[0].pokemon)
                                        PreferencesManager.saveAllObjects(allObjects: self.newListPokemonLike)
                                    } else if drag.translation.width < -self.threshold {
                                        PreferencesManager.removePokemon(id: deck[0].pokemon.id ?? 0)
                                    }
                                    self.updateDeck()
                                }
                    )
            }
        }
        BottomBarView()
            .opacity(self.dragState.isDragging ? 0.1 : 1.0)
            .animation(.linear)
        
            .onAppear {
                var decks = [CardView]()
                
                for _ in 0..<2 {
                    decks.append(CardView(pokemon: self.listPokemon.randomElement()!))
                }
                
                self.deck = decks
            }
    }
    
    private func isTopCard(card: CardView) -> Bool {
        guard let idx = deck.firstIndex(where: {$0.id == card.id}) else {
            return false
        }
        
        return idx == 0
    }
    
    private func updateDeck(){
        deck.removeFirst()
        self.lastCardIdx += 1
        
        let newPokemon = self.listPokemon.randomElement()
        let newCardView = CardView(pokemon: newPokemon!)
        deck.append(newCardView)
        
    }
}

struct TinderView_Previews: PreviewProvider {
    static var previews: some View {
        Group{
            TinderView(listPokemon: [])
            BottomBarView().previewLayout(.fixed(width: 300, height: 100))
        }
    }
}

struct TopBarView: View {
    var body: some View{
        HStack{
            Image(systemName: "line.horizontal.3")
                .font(.system(size:25))
                .foregroundColor(.black)
            Spacer()
            Image(systemName: "book.fill")
                .font(.system(size: 35))
                .foregroundColor(.black)
            Spacer()
            Image(systemName: "archivebox.fill")
                .font(.system(size: 25))
                .foregroundColor(.black)
        }.padding(15)
    }
}

struct BottomBarView: View {
    var body: some View{
        HStack{
            Image(systemName: "xmark")
                .font(.system(size: 25))
                .foregroundColor(.black)
            Spacer()            
            Spacer()
            Image(systemName: "heart")
                .font(.system(size: 25))
                .foregroundColor(.black)
            
        }.padding(15)
    }
}
