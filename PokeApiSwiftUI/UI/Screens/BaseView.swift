//
//  BaseView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 14/03/21.
//

import SwiftUI

struct BaseView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct BaseView_Previews: PreviewProvider {
    static var previews: some View {
        BaseView()
    }
}
