//
//  ListLikeView.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 15/03/21.
//

import SwiftUI

struct ListLikeView: View {
    @State var listPokemon: [Pokemon] = []
    
    var body: some View {
        VStack{
            if listPokemon.isEmpty {
                Text("You haven't favorites Pokemons")
            } else {
                Spacer(minLength: 10)
                Text("Your favorite Pokemon")
                    .font(.title)
                    .bold()
                    .foregroundColor(Color.blue)
                List(self.listPokemon) { poke in
                    Detail(text: poke.name)
                }
            }
        }
        .onAppear {
            self.listPokemon = PreferencesManager.getAllObjects
        }
    }
    
}

struct ListLikeView_Previews: PreviewProvider {
    static var previews: some View {
        ListLikeView()
    }
}
