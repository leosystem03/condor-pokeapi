//
//  Pokemon.swift
//  PokeApp
//
//  Created by Leonard Borrego on 13/03/21.
//

import Foundation

import Foundation

struct PokemonResponse: Codable {
    let count: Int
    let results: [Pokemon]
}

struct Pokemon: Identifiable, Hashable, Codable {
    var id: Int?
    let name: String
    let image: Data?
    var isLike: Bool?
    
    mutating func setLike(isLike: Bool) {
        self.isLike = isLike
    }
}

struct Moves: Hashable,Codable {
    var move: MovesDetail
}

struct MovesDetail: Hashable, Codable {
    var name: String
}

struct Type: Hashable, Codable {
    var name: String
}

struct Types: Hashable, Codable {
    var type: Type
}

struct PokemonDetail:  Codable {
    var name: String
    var height: Int
    var weight: Int
    var order: Int
    var types: [Types]
    var moves: [Moves]
    //var sprites: [Sprites]
}

struct Sprites: Codable {
    
}

