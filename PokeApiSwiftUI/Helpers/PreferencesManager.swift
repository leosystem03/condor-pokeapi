//
//  PreferencesManager.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 15/03/21.
//

import Foundation
import SwiftUI

class PreferencesManager {
    
    static let shared = PreferencesManager()
    
    static var getAllObjects: [Pokemon] {
          let defaultObject = Pokemon(id: 0, name: "sdas", image: Data(), isLike: false)
          if let objects = UserDefaults.standard.value(forKey: "user_objects") as? Data {
             let decoder = JSONDecoder()
             if let objectsDecoded = try? decoder.decode(Array.self, from: objects) as [Pokemon] {
                return objectsDecoded
             } else {
                return [defaultObject]
             }
          } else {
             return [defaultObject]
          }
       }

     static func saveAllObjects(allObjects: [Pokemon]) {
          let encoder = JSONEncoder()
          if let encoded = try? encoder.encode(allObjects){
             UserDefaults.standard.set(encoded, forKey: "user_objects")
          }
     }
    
    static func removePokemon(id: Int) {
        var listPokemon = PreferencesManager.getAllObjects
        listPokemon.removeAll{
            $0.id == id
        }
        
        PreferencesManager.saveAllObjects(allObjects: listPokemon)
    }
}
