//
//  ImageSprites.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import SwiftUI

class ImageSprites: ObservableObject {
    @Published private(set) var data: Data?
    
    func getImage(id: Int) {
        let url = "\(ApiService.baseImg)\(id).png"
        do {
            self.data = try Data(contentsOf: URL(string: url)!)
        } catch {
            print("Error")
        }
    }
}
