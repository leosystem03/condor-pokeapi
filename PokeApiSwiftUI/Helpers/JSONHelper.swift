//
//  JSONHelper.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 14/03/21.
//


import Foundation

extension Dictionary {
    /**
     *  Comvert from `Dictionary` to `Codable`.
     *
     *  - Returns:Dictionary object as Codable
     */
    func deserialize<T>(type: T.Type) -> T? where T: Codable {
        do {
            let data = try JSONSerialization.data(withJSONObject: self,
                                                         options: .prettyPrinted)
            return try JSONDecoder().decode(type, from: data)
        } catch let error {
            print(">> Error Get/Parese as Data Custom =\(error) !!")
        }
        return nil
    }
    
}
