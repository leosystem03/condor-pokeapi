//
//  ApiService.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 13/03/21.
//

import Foundation

class ApiService {
    
    static let baseUrl = "https://pokeapi.co/api/v2/pokemon?limit="
    static let baseImg = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
    static let detail  = "https://pokeapi.co/api/v2/pokemon/"
    static let red_blue = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-i/red-blue/"
    static let emerald = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-iii/emerald/"
    static let ruby = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/versions/generation-ii/crystal/"
    
    typealias parameters = [String: Any]
    
    enum HTTPMethod: String {
        case get        = "GET"
        case post       = "POST"
        case connect    = "CONNECT"
        case delete     = "DELETE"
        case put        = "PUT"
    }
    
    enum Result: Error {
        case success(Dictionary<String, Any>)
        case failure(ErrorRequest)
    }
    
    enum ErrorRequest: Error {
        case connectionError(message: String)
        case unknownError(message: String)
        case authorizationError(message: String, Data)
        case serverError(message: String)
        case jsonError(message: String)
    }
    
    static func consumeService(baseUrl:String,
                        method: HTTPMethod = .get,
                        parameters: parameters? = nil,
                        completion: @escaping (Result) -> Void){
        
        let header = ["Content-Type" : "application/json; charset=utf-8"]

        var request = URLRequest(url: URL(string: baseUrl)!,
                                 timeoutInterval: 10)
        request.allHTTPHeaderFields = header
        request.httpMethod = method.rawValue
        
        if let parameters = parameters {
            let parameterData = parameters.reduce("") { (result, param) -> String in
                return result + "&\(param.key)=\(param.value as! String)"
            }.data(using: .utf8)
            request.httpBody = parameterData
        }
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                do {
                    if let error = error {
                        print(error)
                        completion(Result.failure(.connectionError(message: error.localizedDescription)))
                    } else if let data = data, let responseCode = response as? HTTPURLResponse {
                        switch responseCode.statusCode {
                        case 200:
                            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Any> {
                                completion(Result.success(convertedJsonIntoDict))
                           }
                        case 400...499:
                            completion(Result.failure(.authorizationError(message: responseCode.description ,data)))
                        case 500...599:
                            completion(Result.failure(.serverError(message: responseCode.description)))
                        default:
                            completion(Result.failure(.unknownError(message: responseCode.description)))
                            break
                        }
                    }
                } catch {
                    
                }
            }
        }.resume()
    }
    
    static func cancel() {
        URLSession.shared.invalidateAndCancel()
    }
}

