//
//  Extensions.swift
//  PokeApiSwiftUI
//
//  Created by Leonard Borrego on 15/03/21.
//

import Foundation
import SwiftUI

extension AnyTransition {
    static var trailingBottomAtRemoval: AnyTransition {
        AnyTransition.asymmetric(insertion: .identity,
                                 removal: AnyTransition.move(edge: .trailing).combined(with: .move(edge: .bottom)))
    }
    
    static var leadingBottomAtRemoval: AnyTransition {
        AnyTransition.asymmetric(insertion: .identity,
                                 removal: AnyTransition.move(edge: .leading).combined(with: .move(edge: .bottom)))
    }
}

